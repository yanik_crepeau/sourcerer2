require 'test_helper'

class NewsSourcesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  fixtures :users, :roles, :users_roles


  setup do
    @admin_user = User.last
    @manager_user = User.first
    @news_source = news_sources(:one)
  end

  # get index group
  test "should get index (admin)" do
    sign_in @admin_user
    get news_sources_url
    assert_response :success
  end

  test "should get index (manager)" do
    sign_in @manager_user
    get news_sources_url
    assert_response :success
  end

  test "should get index (signed_out)" do
    get news_sources_url
    assert_response :success
  end

  # get new group
  test "should get new (admin)" do
    sign_in @admin_user
    get new_news_source_url
    assert_response :success
  end

  test "should get new (manager)" do
    sign_in @manager_user
    get new_news_source_url
    assert_response :success
  end

  test "should NOT get new (signed_out)" do
    get new_news_source_url
    assert_response :redirect
  end

  # post create group
  test "should create news_source (admin)" do
    sign_in @admin_user
    assert_difference('NewsSource.count') do
      post news_sources_url, params: { news_source: { description: @news_source.description, full_address: @news_source.full_address, latitude: @news_source.latitude, longitude: @news_source.longitude, name: @news_source.name, url: @news_source.url } }
    end
    assert_redirected_to news_source_url(NewsSource.last)
  end

  test "should create news_source (manager)" do
    sign_in @manager_user
    assert_difference('NewsSource.count') do
      post news_sources_url, params: { news_source: { description: @news_source.description, full_address: @news_source.full_address, latitude: @news_source.latitude, longitude: @news_source.longitude, name: @news_source.name, url: @news_source.url } }
    end
    assert_redirected_to news_source_url(NewsSource.last)
  end

  test "should NOT create news_source (signed_out)" do
    assert_no_difference('NewsSource.count') do
      post news_sources_url, params: { news_source: { description: @news_source.description, full_address: @news_source.full_address, latitude: @news_source.latitude, longitude: @news_source.longitude, name: @news_source.name, url: @news_source.url } }
    end

    assert_redirected_to news_sources_url
  end

  # get show group
  test "should show news_source (admin)" do
    sign_in @admin_user
    get news_source_url(@news_source)
    assert_response :success
  end

  test "should show news_source (manager)" do
    sign_in @manager_user
    get news_source_url(@news_source)
    assert_response :success
  end

  test "should show news_source (signed_out)" do
    get news_source_url(@news_source)
    assert_response :success
  end

  # get edit group
  test "should get edit (admin)" do
    sign_in @admin_user
    get edit_news_source_url(@news_source)
    assert_response :success
  end

  test "should get edit (manager)" do
    sign_in @manager_user
    get edit_news_source_url(@news_source)
    assert_response :success
  end

  test "should NOT get edit (signed_out)" do
    get edit_news_source_url(@news_source)
    assert_response :redirect
  end

  # patch update group
  test "should update news_source (admin)" do
    sign_in @admin_user
    patch news_source_url(@news_source), params: { news_source: { description: @news_source.description, full_address: @news_source.full_address, latitude: @news_source.latitude, longitude: @news_source.longitude, name: @news_source.name, url: @news_source.url } }
    assert_redirected_to news_source_url(@news_source)
  end

  test "should update news_source (manager)" do
    sign_in @manager_user
    patch news_source_url(@news_source), params: { news_source: { description: @news_source.description, full_address: @news_source.full_address, latitude: @news_source.latitude, longitude: @news_source.longitude, name: @news_source.name, url: @news_source.url } }
    assert_redirected_to news_source_url(@news_source)
  end

  test "should update news_source (signed_out)" do
    patch news_source_url(@news_source), params: { news_source: { description: @news_source.description, full_address: @news_source.full_address, latitude: @news_source.latitude, longitude: @news_source.longitude, name: @news_source.name, url: @news_source.url } }
    assert_redirected_to news_sources_url
  end



  #delete group - Note: only difference between
  #admin (can delete) and manager (can NOT delete)
  test "should destroy news_source (admin)" do
    sign_in @admin_user
    assert_difference('NewsSource.count', -1) do
      delete news_source_url(@news_source)
    end

    assert_redirected_to news_sources_url
  end

  test "should NOT destroy news_source (manager)" do
    sign_in @manager_user
    assert_no_difference('NewsSource.count', -1) do
      delete news_source_url(@news_source)
    end

    assert_redirected_to news_sources_url
  end

  test "should NOT destroy news_source (signed_out)" do
    assert_no_difference('NewsSource.count', -1) do
      delete news_source_url(@news_source)
    end

    assert_redirected_to news_sources_url
  end



end
