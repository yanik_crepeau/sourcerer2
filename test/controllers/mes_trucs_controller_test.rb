require 'test_helper'

class MesTrucsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    #puts "mes_trucs_controller test setup"
    sign_in User.last
    @mes_truc = mes_trucs(:one)
  end

  test "should get index" do
    get mes_trucs_url
    assert_response :success    
  end

  test "should get new" do
    get new_mes_truc_url
    assert_response :success
  end

  test "should create mes_truc" do
    assert_difference('MesTruc.count') do
      post mes_trucs_url, params: { mes_truc: { artist: @mes_truc.artist, title: @mes_truc.title } }
    end

    assert_redirected_to mes_truc_url(MesTruc.last)
  end

  test "should show mes_truc" do
    get mes_truc_url(@mes_truc)
    assert_response :success
  end

  test "should get edit" do
    get edit_mes_truc_url(@mes_truc)
    assert_response :success
  end

  test "should update mes_truc" do
    patch mes_truc_url(@mes_truc), params: { mes_truc: { artist: @mes_truc.artist, title: @mes_truc.title } }
    assert_redirected_to mes_truc_url(@mes_truc)
  end

  test "should destroy mes_truc" do
    assert_difference('MesTruc.count', -1) do
      delete mes_truc_url(@mes_truc)
    end

    assert_redirected_to mes_trucs_url
  end
end
