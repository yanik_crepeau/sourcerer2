require 'test_helper'

class NewsRatingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @news_rating = news_ratings(:one)
  end

  test "should get index" do
    get news_ratings_url
    assert_response :success
  end

  test "should get new" do
    get new_news_rating_url
    assert_response :success
  end

  test "should create news_rating" do
    assert_difference('NewsRating.count') do
      post news_ratings_url, params: { news_rating: { org_id: @news_rating.org_id, rating: @news_rating.rating, source_id: @news_rating.source_id, user_id: @news_rating.user_id } }
    end

    assert_redirected_to news_rating_url(NewsRating.last)
  end

  test "should show news_rating" do
    get news_rating_url(@news_rating)
    assert_response :success
  end

  test "should get edit" do
    get edit_news_rating_url(@news_rating)
    assert_response :success
  end

  test "should update news_rating" do
    patch news_rating_url(@news_rating), params: { news_rating: { org_id: @news_rating.org_id, rating: @news_rating.rating, source_id: @news_rating.source_id, user_id: @news_rating.user_id } }
    assert_redirected_to news_rating_url(@news_rating)
  end

  test "should destroy news_rating" do
    assert_difference('NewsRating.count', -1) do
      delete news_rating_url(@news_rating)
    end

    assert_redirected_to news_ratings_url
  end
end
