Leaflet.tile_layer = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
#Leaflet.tile_layer = '//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'

# You can also use any other tile layer here if you don't want
#to use Cloudmade - see http://leafletjs.com/reference.html#tilelayer
#for more
Leaflet.attribution = "Copyright openstreetmap contributors"
Leaflet.subdomains = ['a', 'b', 'c']
Leaflet.max_zoom = 18
