RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'UsersRole' do
    navigation_label 'Devise'
    #visible false
    weight (-2)
  end

  config.model "User" do
    parent UsersRole
    navigation_icon 'icon-user'
    weight (-1)
  end

  config.model "Role" do
    parent UsersRole
    navigation_icon 'icon-wrench'
    weight (-1)
  end

  config.model "NewsSource" do
    edit do
      exclude_fields :roles
    end
  end

  config.model "NewsCategory" do
    edit do
      exclude_fields :roles
    end
  end

  config.model "MesTrucs" do
    edit do
      exclude_fields :roles
    end
  end
end
