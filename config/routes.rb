Rails.application.routes.draw do
  resources :news_ratings
  resources :orgs
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :news_sources
  resources :news_categories
  get 'about', to: 'about#show'

  resources :mes_trucs
  get 'home/index'
  root 'news_sources#index'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
