ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.

# Added by yjc 2016-09-23
if ENV['HEROKU_RELEASE_VERSION']
        $deploy_version = 'heroku ' + ENV['HEROKU_RELEASE_VERSION']
    else
        $deploy_version = 'local'
end
