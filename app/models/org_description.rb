class OrgDescription < ApplicationRecord
  belongs_to :org

  scope :user_locale,     -> { where("lang = ?", I18n.locale)}
  scope :default_locale,  -> { where("lang = ?", I18n.default_locale)}
end
