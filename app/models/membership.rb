class Membership < ApplicationRecord
  belongs_to :user
  belongs_to :org

  scope :member,  -> { where("credibility >= ?",  10)}
  scope :rater,   -> { where("credibility >= ?", 100)}
  scope :manager, -> { where("credibility >= ?", 500)}
  scope :admin,   -> { where("credibility >= ?", 700)}
end
