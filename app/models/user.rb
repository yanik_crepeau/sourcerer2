class User < ApplicationRecord
  rolify
  has_many :memberships
  has_many :orgs, through: :memberships
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, presence: true
  def name
    email
  end
end
