class Org < ApplicationRecord
  resourcify

  has_many :memberships
  has_many :users, through: :memberships

  has_many :org_ratings
  has_many :news_sources, through: :org_ratings

  has_many :org_descriptions
  accepts_nested_attributes_for :org_descriptions, :allow_destroy => true

  validates :name, presence: true

end
