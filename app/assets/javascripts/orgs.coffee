setup_field_handlers = () ->
  console.log('orgs.coffee setup')
  $('.remove_fields').on 'click', (e) ->
    myfields = $(e.target).closest(".row")
    myfields.find(".destroy input[type=hidden]").val("true")
    myfields.hide()

  $('.add_fields').on 'click', (e) ->

    association= jQuery(this).data('association')
    content = jQuery(this).data('content')
    new_id = '';
    regexp = new RegExp("new_" + association, "g")
    jQuery(this).before(content.replace(regexp, new_id))
    $(this).attr('disabled', true)

$ ->
    console.log('Loading page org.coffee')
    setup_field_handlers()

    document.addEventListener "turbolinks:load", () ->
      console.log('turbolink:load in orgs.coffee')
      setup_field_handlers()
