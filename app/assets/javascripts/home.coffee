setup = ->
  console.log 'home.coffee setup **'
  #When the modal pop up (show.bs.modal), load its content
  ## Called from non-logged in user (deprecated - used in hom)
  $('#presentationModal').on 'show.bs.modal', (e) ->
    # Clearing content
    $('#divRss').text("-")

    $('.modal-title').text('Loading...')


    #Getting record from local
    console.log("Getting data from database")
    $.ajax
      url: jQuery(e.relatedTarget).data('sourceurl')
    .success (data) ->
      $('.modal-title').text(data.name)
      if data.source_url && data.source_url.length > 5
        #Getting data from remote location
        console.log("Calling FeeeEk")
        $('#divRss').FeedEk
          FeedUrl: data.source_url
          MaxCount : 7,
          ShowDesc : true
          ShowPubDate:true
          DescCharacterLimit:250
          TitleLinkTarget:'_blank'
      else
        console.log("No RSS loading, invalid URL")
    .error (jqXHR, textStatus, errorThrown) ->
      $('.modal-title').text("No data available")
      error_report = "<PRE>status: " + textStatus + "\n" +
        errorThrown + "</PRE>"
      $('.modal-body p').html(error_report)

  ## Setting title
  title = document.title
  title = title.substr(0,title.indexOf('|')) if title.indexOf('|') >= 0

  ## Called from logged-in user within the news_source/index.html.haml page
  $('#rssModal') .on 'show.bs.modal', (e) ->
    # Clearing content
    $('#divRss').text("-")
    $('.modal-title').text('Loading...')


    #Getting record from local
    console.log("Getting data from local database")
    source_url = jQuery(e.relatedTarget).data('sourceurl')
    source_name = jQuery(e.relatedTarget).data('sourcename')
    source_id = $(e.relatedTarget).data('sourceid')
    $('#divVote').attr('data-sourceid', source_id)


    if source_name
      $('.modal-title').text(source_name)

    if source_url && source_url.length > 5
      $('.nav-tabs a[href="#divRss"]').tab('show');
      #Getting data from remote location
      console.log("Calling FeeeEk with #{source_url}")
      try
        $('#divRss').FeedEk
          FeedUrl: source_url
          MaxCount : 7,
          ShowDesc : true
          ShowPubDate:true
          DescCharacterLimit:250
          TitleLinkTarget:'_blank'
      catch error
        console.log("Exception raised")
        $('.modal-body p').html(error)


$ ->
  console.log("Loading page home.coffee...!  " + Date.now()%10000)
  setup()

  document.addEventListener "turbolinks:load", () ->
    console.log('turbolink:load in home.coffee')
    setup()
