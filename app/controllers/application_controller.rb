class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale

  def supported_locales
    supported_locales = ['fr', 'en']
    supported_locales
  end

  private

  def set_locale
    if params["set_locale"] != nil && (params["set_locale"].in? supported_locales) then
      # Top priority: if the page is called with the 'set_locale' parameter
      I18n.locale = params["set_locale"]
      session["set_locale"] = params["set_locale"]
      #puts "using url param for I18: #{params['set_locale']}"
    elsif session['set_locale'] != nil
      # 2nd priority: if I have a session token
      I18n.locale = session['set_locale']
    else
      # 3rd priority: Try to use the browser language setting
      extracted = extract_locale_from_accept_language_header
      if extracted.in? supported_locales then
        # The browser returned a supported language
        I18n.locale = extracted
      else
        # OK, used the default_locale (en)
        I18n.locale = I18n.default_locale
      end
    end
  end

  # Check in the request if I have a HTTP_ACCEPT_LANGUAGE header.
  # One issue: When in test mode, no browser can return HTTP_ACCEPT_LANGUAGE
  # header, so, use I18n.default_locale
  def extract_locale_from_accept_language_header
    begin
      request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    rescue
      I18n.default_locale
    end
  end
end
