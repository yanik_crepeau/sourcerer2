class NewsRatingsController < ApplicationController
  before_action :set_news_rating, only: [:show, :edit, :update, :destroy]

  # GET /news_ratings
  # GET /news_ratings.json
  def index
    source_id = params[:source_id]
    if !source_id.nil? then
      @news_ratings = NewsRating.where('user_id = ? AND news_source_id = ?',
        current_user.id,
        source_id).order(:org_id)
        #Check if 'personnal' exists
        rating = @news_ratings.select {|r| r['org_id'] == nil}
        if rating.count == 0 then
          puts "does not exist for personnal"
          new_rating = NewsRating.new({user_id: current_user.id, news_source_id:source_id, org_id: nil, rating:0})
          if new_rating.save()
            @news_ratings.reload()
          end
        end

        #Then check for each orgs the user belongs to
        orgs = current_user.orgs
        orgs.map do |org|
          rating = @news_ratings.select {|r| r['org_id'] == org.id}
          if rating.count == 0 then
            puts "does not exist for #{org.name}"
            new_rating = NewsRating.new({user_id: current_user.id, news_source_id:source_id, org_id: org.id, rating:0})
            if new_rating.save()
              @news_ratings.reload()
            end
          end
        end
    else
      @news_ratings = NewsRating.where('user_id = ?', current_user.id)
    end
  end

  # GET /news_ratings/1
  # GET /news_ratings/1.json
  def show
  end

  # GET /news_ratings/new
  def new
    @news_rating = NewsRating.new
  end

  # GET /news_ratings/1/edit
  def edit
  end

  # POST /news_ratings
  # POST /news_ratings.json
  def create
    @news_rating = NewsRating.new(news_rating_params)

    respond_to do |format|
      if @news_rating.save
        format.html { redirect_to @news_rating, notice: 'News rating was successfully created.' }
        format.json { render :show, status: :created, location: @news_rating }
      else
        format.html { render :new }
        format.json { render json: @news_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news_ratings/1
  # PATCH/PUT /news_ratings/1.json
  def update
    puts "received params: #{params}"
    respond_to do |format|
      if @news_rating.update(news_rating_params)
        format.html { redirect_to @news_rating, notice: 'News rating was successfully updated.' }
        format.json { render :show, status: :ok, location: @news_rating }
      else
        format.html { render :edit }
        format.json { render json: @news_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news_ratings/1
  # DELETE /news_ratings/1.json
  def destroy
    @news_rating.destroy
    respond_to do |format|
      format.html { redirect_to news_ratings_url, notice: 'News rating was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news_rating
      @news_rating = NewsRating.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def news_rating_params
      params.require(:news_rating).permit(:source_id, :org_id, :user_id, :rating)
    end
end
