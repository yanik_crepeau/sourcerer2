##
# This classe is used to build the json response required
# by the client side (javascript) DataTable object when it
# fires an Ajax call.
##
class NewsSourcesDatatable
  delegate :params, :link_to, :number_to_currency, :t1, :t2, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho:                  params[:sEcho].to_i,
      iTotalRecords:          NewsSource.count,
      iTotalDisplayRecords:   news_sources.total_entries,
      aaData:                 data
    }
  end

private

  def data
    news_sources.map do |news_source|
      [
        link_to(news_source.name, news_source),
        rating(news_source.id),
        ERB::Util.json_escape(news_source.url),
        ERB::Util.json_escape(news_source.description),
        ERB::Util.json_escape(news_source.full_address),
        ERB::Util.json_escape(news_source.name),
        ERB::Util.json_escape(news_source.lang),
        ERB::Util.json_escape(news_source.id)
      ]
    end
  end 

  def rating(source_id)
    "<p>#{t1(source_id)}</p><p>#{t2(source_id)}</p>"
  end

  def news_sources
    @news_sources ||= fetch_news_sources
  end

  def fetch_news_sources
    news_sources = NewsSource.order("#{sort_column} #{sort_direction}")
    news_sources = news_sources.page(page).per_page(per_page)
    if params[:sSearch].present?
      news_sources = news_sources.where("name like :search or full_address like :search", search: "%#{params[:sSearch]}%")
    end
    news_sources
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 5
  end

  def sort_column
    columns = %w[name url description full_address sname]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
