class HomeController < ApplicationController
  def index
    if user_signed_in?
      redirect_to news_sources_url
    else
      @news_sources = NewsSource.take(15)
    end
  end
end
