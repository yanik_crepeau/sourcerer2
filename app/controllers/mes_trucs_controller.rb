class MesTrucsController < ApplicationController
  #before_action :set_mes_truc, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to mes_trucs_path, :alert => exception.message
  end

  # GET /mes_trucs
  # GET /mes_trucs.json
  def index
    @mes_trucs = MesTruc.paginate(:page => params[:page], per_page: 5)
    puts "index /mes_trucs"
  end

  # GET /mes_trucs/1
  # GET /mes_trucs/1.json
  def show
    puts "show /mes_trucs"
  end

  # GET /mes_trucs/new
  def new
    @mes_truc = MesTruc.new
  end

  # GET /mes_trucs/1/edit
  def edit
  end

  # POST /mes_trucs
  # POST /mes_trucs.json
  def create
    @mes_truc = MesTruc.new(mes_truc_params)

    respond_to do |format|
      if @mes_truc.save
        format.html { redirect_to @mes_truc, notice: 'Mes truc was successfully created.' }
        format.json { render :show, status: :created, location: @mes_truc }
      else
        format.html { render :new }
        format.json { render json: @mes_truc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mes_trucs/1
  # PATCH/PUT /mes_trucs/1.json
  def update
    respond_to do |format|
      if @mes_truc.update(mes_truc_params)
        format.html { redirect_to @mes_truc, notice: 'Mes truc was successfully updated.' }
        format.json { render :show, status: :ok, location: @mes_truc }
      else
        format.html { render :edit }
        format.json { render json: @mes_truc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mes_trucs/1
  # DELETE /mes_trucs/1.json
  def destroy
    @mes_truc.destroy
    respond_to do |format|
      format.html { redirect_to mes_trucs_url, notice: 'Mes truc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mes_truc
      @mes_truc = MesTruc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mes_truc_params
      params.require(:mes_truc).permit(:title, :artist)
    end
end
