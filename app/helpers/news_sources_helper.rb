module NewsSourcesHelper
  def t1(source_id)
    if current_user.nil? then
      "-"
    else
      @news_ratings = NewsRating.where('user_id = ? AND news_source_id = ?',
      current_user.id,
      source_id)
      rating = @news_ratings.select {|r| r['org_id'] == nil}
      if (!rating.nil? && !rating.first.nil?) then
        r = rating.first.rating
        if (r > 0) then
          u(r)
        elsif (r == 0)
          "0"
        elsif (r < 0)
          d(r.abs)
        end
      else
        "-"
      end
    end
  end

  def t2(source_id)
    "ADN: " + d(-5.abs)
  end

  private


  def d(rate)
    "#{rate} <span class='fa fa-sm fa-thumbs-down'></span>"
  end

  def u(rate)
    "#{rate} <span class='fa fa-sm fa-thumbs-up'></span>"
  end

end
