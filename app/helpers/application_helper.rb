module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = t "site_name", default:"Is it true? - News Source Rater"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
    #puts "Setting title page_title: #{page_title}"
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  def datatable_lang_url
    code = 'en'
    code = I18n.locale
    #puts "*** datatable_lang_url: -#{I18n.locale}- code: -#{code}-"
    "datatable_#{code}.ison"
  end

  # LanguageList::COMMON_LANGUAGES

  def locale_languages
    ret = [
      build_hash_language(:en, true),
      build_hash_language(:fr, true),
    ]

    [:es, :ru, :zh, :ar, :de, :it, :ja, :pt, :nl].sort.each {|lang|
        ret << build_hash_language(lang,false)
    }

    ret
  end

  def build_hash_language(language = :en, enabled = false)
    if false && language == I18n.locale then
      ret = {
        label: t(language, :scope => :languages).capitalize,
        locale: language,
        disabled: !enabled
      }
    else
      ret = {
        label: "#{t(language, :scope => :languages).capitalize} | #{t(language, :scope => :languages, :locale => language).capitalize}",
        locale: language,
        disabled: !enabled
      }
    end
    #puts "#{ret[:locale]} --->> #{ret[:label]}"
    ret
  end

  def should_print_warning?
    retVal = true
    lastPrinted = session['lastPrinted']
    timeNow = Time.now.to_i


    if (lastPrinted != nil) then
      timeLimit = lastPrinted + 60.minutes
      if (timeNow > timeLimit) then
        retVal = true
        session['lastPrinted'] = timeNow
      else
        retVal = false
      end
    else
      retVal = true
      session['lastPrinted'] = timeNow
    end

    retVal
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    new_object.id = "new_#{association}"
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |d|
      render(association.to_s + "_form", :f => f, :d => new_object)
    end
    #puts "Fields: #{fields}"
    #link_to_function(name, h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"))
    button_tag name, :type => 'button',
      :class => 'btn btn-danger add_fields',
      :data => {
        :association => association,
        :content => (fields)
      }
  end



end
