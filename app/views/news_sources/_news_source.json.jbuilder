json.extract! news_source, :id, :name,  :description, :full_address, :longitude, :latitude, :created_at, :updated_at
json.url news_source_url(news_source, format: :json)
json.source_url news_source.url
