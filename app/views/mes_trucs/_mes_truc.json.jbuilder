json.extract! mes_truc, :id, :title, :artist, :created_at, :updated_at
json.url mes_truc_url(mes_truc, format: :json)