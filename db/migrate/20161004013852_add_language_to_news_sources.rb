class AddLanguageToNewsSources < ActiveRecord::Migration[5.0]
  def change
    add_column :news_sources, :lang, :string

    NewsSource.connection.execute("UPDATE news_sources SET lang = 'en'")
  end
end
