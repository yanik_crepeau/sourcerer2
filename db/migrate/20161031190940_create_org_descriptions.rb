class CreateOrgDescriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :org_descriptions do |t|
      t.integer :org_id
      t.string :lang, :limit => 8
      t.text :description, :limit => 2000

      t.timestamps
      t.index [:org_id,:lang], name: 'org_lang_index_for_org_descriptions'
    end

  end
end
