class CreateNewsRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :news_ratings do |t|
      t.references :news_source
      t.references :org
      t.references :user
      t.integer :rating

      t.timestamps
    end
  end
end
