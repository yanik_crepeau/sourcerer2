class CreateOrgRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :org_ratings do |t|
      t.references :news_source, foreign_key: true
      t.references :org, foreign_key: true
      t.integer :rating
      t.timestamps
    end
  end
end
