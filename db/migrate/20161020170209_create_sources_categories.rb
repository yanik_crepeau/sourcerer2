class CreateSourcesCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :news_categories_news_sources, id: false do |t|
      t.integer :news_source_id
      t.integer :news_category_id
      t.index [:news_source_id, :news_category_id], name: "index_news_sources_news_categoies_on_source_and_category"
    end
  end
end
