
# Project page

![hello](https://latex.codecogs.com/gif.latex?%5Ccos%5E%7B-1%7D%5Ctheta%5Csin%5E%7B-1%7D%5Ctheta%20%5Cleft%28%5Cfrac%7B%5Cpi%7D%7B2%7D-%5Ctheta%20%5Cright%20%29)

# Table of content

* [Good practices](#markdown-header-good-practices-on-a-mac)
* [Bootstrap](#markdown-header-bootstrap)
* [Haml](#markdown-header-haml)
* [Bower](#markdown-header-bower)
* [I18n](#markdown-header-internationalization-i18n)
  * [General translations](#markdown-header-general-translations)
  * [Devis translations](#markdown-header-devise-translations)
  * [Datatable translations](#markdown-header-datatable-translations)
  * [Haml and Ruby files](#markdown-header-general-considerations)
    * [Helper Ruby](#markdown-header-the-helper)
    * [View Haml](#markdown-header-the-view-files-htmlhaml)
    * [Controller](#markdown-header-the-application-controller)

![Energize](https://latex.codecogs.com/gif.latex?%5Cdpi%7B300%7D%20e%3Dm%20c%5E2)

![Web development](https://www.dropbox.com/s/065coi935meyuov/Capture%20d%27%C3%A9cran%202016-10-09%2001.27.25.png?raw=1)

This is the  project with the following features:

* Uses gem [devise](https://github.com/plataformatec/devise) to handle user login
* Uses gem [rolify](https://github.com/RolifyCommunity/rolify) to manage different roles
* Uses gem [cancancan](https://github.com/CanCanCommunity/cancancan) inside models/view/controller to actually implements roles and privileges.


##Good practices (on a Mac)

* Use [homebrew](http://brew.sh/) to download and install exotic libraries on your Mac
* Use [rvm (Ruby version manager)](https://rvm.io/) to handle different Ruby versions.
* Edit your .bashrc file (in your home directory to add)

````
alias r='rails'
alias project_name='cd PATH OF YOUR PROJECT'
alias src2='cd /Users/ycrepeau/Document/Development/rolify_tutorial'
````

Once completed, run the 'source ~/.bashrc' on the terminal to reload these
new features. You just have to type 'r' instead of 'rails' to launch rails or type the project shortcut ('src2') to jump directlu in your project directory.

\[[Back](#markdown-header-table-of-content)\]

##Bootstrap

This project uses bootstrap to give a nice look and feel to the
gui interface. When scaffolding a new ressource, the [gem bootstre-sass-extra](https://github.com/doabit/bootstrap-sass-extras)
helps you to create a nicer look and feel.

````
  r g scaffold ressourceName field1:string, field2:date...
  r g bootstrap:themed ressourceName
````
\[[Back](#markdown-header-table-of-content)\]

##Haml

To make a batch conversion from .erb to .haml, uses the following commands:

````
find . -name \*.erb -print | sed 'p;s/.erb$/.haml/' | xargs -n2 html2haml
find . -name \*.erb -delete
````

The first command creates the .haml files and the second get rid of the .erb ones.

\[[Back](#markdown-header-table-of-content)\]

##Bower

This project uses Bower. The Gemfile conains the instructions about how
to update vendor asset packages.

At this time, the Bowerfile contains the following asset imports:

* Font-awesome

\[[Back](#markdown-header-table-of-content)\]

#Development (2016-10-06)

## Internationalization (I18n)

This project uses the I18n library to handle translations in
French and English. The translation issues fall in 3 categories:

1. General tranlations
2. Devise (imported gem) translations
3. Datatable (imported js - client side) translations

\[[Back](#markdown-header-table-of-content)\]

### General translations

In config/locales, I have placed two files:

* en.yml
* fr.yml

These files contain translations for the strings under
my control (aka in my code).

\[[Back](#markdown-header-table-of-content)\]

### Devise translations

In config/locale, I have placed two files, downloaded from
the Internet (Devise web site):

* devise.en.yml
* devise.fr.yml

\[[Back](#markdown-header-table-of-content)\]

### Datatable translations

This is a special case. The datatable code does not "live"
on the server side but on the client side (jQuery module).
The translations table has to be loaded by the browser when
the datatable instance is created by jQuery. Furthermore, the
format is not .yml at all. The *.ison file looks very much like
a *.json dialect.

I have placed 2 files in /public to be served when jQuery/Datatable
on the client side request one of them:

* datatable_en.ison
* datatable_fr.ison

\[[Back](#markdown-header-table-of-content)\]

### General considerations

Once every files have been placed in /config/locales and /public,
I have modified the following files:

* /app/controller/application.rb
* /app/helper/application.rb.
* /app/view/layouts/_header.html.haml
* /app/view/layouts/_locale_selector.html.haml (new file)

\[[Back](#markdown-header-table-of-content)\]

#### The helper

The helper has few new methods:

```ruby
#/app/herpers/application_helper.rb
def datatable_lang_url
  code = 'en'
  code = I18n.locale
  "datatable_#{code}.ison"
end
```

Is used to build the correct file name to be used when
creating the datatable (jQuery) object.

The coffescript used to build the datatable looks:

```coffeescript
# app/views/news_sources/index.html.haml
# Client side code calling support language file
jQuery ->
  $('#data_sources').dataTable
    #...
    language: {
      url: "#{datatable_lang_url}"
    }

```

Few other addition/modification have be done in this file
to achieve a better I18n support.

```ruby
#/app/herpers/application_helper.rb
# Returns the full title on a per-page basis.
def full_title(page_title = '')
  base_title = t "site_name", default:"Is it true? - News Source Rater"
  if page_title.empty?
    base_title
  else
    page_title + " | " + base_title
  end
end
```

This method uses a call to the 't' method (a shorthand for
translate) to get the site_name from the translation files.
If the translation is not available, the default: is used.

```ruby
#/app/herpers/application_helper.rb
def locale_languages
  [
    { label: t('locale_selector.en'), locale: 'en' },
    { label: t('locale_selector.fr'), locale: 'fr' },
  ]
end
```

This method builds a hash table to handle the supported
languages used by the drop down language picker.

\[[Back](#markdown-header-table-of-content)\]

#### The view files (html/haml)

```haml
-# /app/view/layouts/_header.html.haml
.navbar-collapse.navbar-inverse.collapse.navbar-ex1-collapse
  %ul.nav.navbar-nav.navbar-right
    %li= link_to "Home", root_path
    %li= link_to "Mes trucs",  mes_trucs_path
    - if user_signed_in?
      %li= link_to "News sources", news_sources_path
      %li= link_to "Logout",  destroy_user_session_path, :method => :delete
    - else
      %li=link_to "Log in!", new_user_session_path

    %li= render 'layouts/locale_selector', controller: controller_name, action: action_name
```


Only the last line is interesting here. It adds a new item in the
menu bar and delegates the whole thing to the other file.


```haml
-# /app/view/layouts/_locale_selector.html.haml
%li#fat-menu.dropdown
  %a.dropdown-toggle{"data-toggle" => "dropdown", href: "#"}
    //= t('.language')
    %span.fa.fa-lg.fa-globe
    %b.caret
  %ul.dropdown-menu
    - locale_languages.each do |language|
      %li
        = link_to language[:label],
                  controller: controller,
                  action: action,
                  set_locale: language[:locale],
                  page: params[:page]

```

I have replaced the name of the menu (Language) with a
globe icon (%span.fa.fa-lg.fa-globe) using the font awesome
font. The call - local_language.each... loops over the supported
languages and actually build the drop down.
Note the action: and controller: parameters. Basically, the
current page is reloaded adding a 'set_locale' and 'page'
parameters in the query. I don't know when the page parameter
is there... but it was in the example so I decided to keeep it.

\[[Back](#markdown-header-table-of-content)\]

#### The application controller

The magic is performed at the begining of the file. When a new
page is loaded, the before_filter is called:

```ruby
#/app/controllers/application_controller.rb
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_locale
  #...
end
```

The set_local method performs the actual trick. It uses
three ways to determine which language should be used:

1. The choice made by the user with the to bar menu
2. Some data stored in the session cookie
3. The info provide the the client browser.
4. In last ressort, the default_locale (en) is used.

```ruby
def set_locale

  supported_locales = ['fr', 'en']

  if params["set_locale"] != nil && (params["set_locale"].in? supported_locales) then
    # Top priority: if the page is called with the 'set_locale' parameter
    I18n.locale = params["set_locale"]
    session["set_locale"] = params["set_locale"]
    #puts "using url param for I18: #{params['set_locale']}"
  elsif session['set_locale'] != nil
    # 2nd priority: if I have a session token
    I18n.locale = session['set_locale']
  else
    # 3rd priority: Try to use the browser language setting
    extracted = extract_locale_from_accept_language_header
    if extracted.in? supported_locales then
      # The browser returned a supported language
      I18n.locale = extracted
    else
      # OK, used the default_locale (en)
      I18n.locale = I18n.default_locale
    end
  end
end

# Check in the request if I have a HTTP_ACCEPT_LANGUAGE header.
def extract_locale_from_accept_language_header
  request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
end
```

\[[Back](#markdown-header-table-of-content)\]

# Heroku - Mailgun

The mailgun has been provisioned with the DNS credential. The site can now send e-mails to anyone registering.

#Thanks

Math Formula written using

![LaTeX](https://latex.codecogs.com/gif.latex?%5Cdpi%7B300%7D%20%5Chuge%20%5CLaTeX)

and processed with https://www.codecogs.com/latex/eqneditor.php

## OK2


